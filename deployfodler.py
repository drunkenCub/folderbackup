#****************************
# Deployment Script by Embla
#****************************
#****************************
#****************************
#CONFIG
FOLDER_LIST = ["bin", "Content", "Scripts", "Views"] #Folder List to be deployed
#****************************
#****************************
#****************************
#*******************************************************************************


# COMMAND LINE USAGE 
# python deployfodler.py SITENAME_AS_IN_CONFIG_PY
 
#******************************************************************************* 

import os
import shutil
import copyf
import sys
import config


for fol in FOLDER_LIST:
	#print(config.COPY_DIR_SRC[sys.argv[1]])
	copyf.copyfiles(fol, sys.argv[1])
	



