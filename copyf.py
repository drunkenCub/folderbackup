import os
import shutil
import config

def copyfiles(directory, sitename): 
	root_src_dir = config.COPY_DIR_SRC[sitename] + directory
	root_dst_dir = config.COPY_DIR_DEST[sitename] + directory
	for src_dir, dirs, files in os.walk(root_src_dir):		
		dst_dir = src_dir.replace(root_src_dir, root_dst_dir)
		if not os.path.exists(dst_dir):
			os.mkdir(dst_dir)
		for file_ in files:
			src_file = os.path.join(src_dir, file_)
			dst_file = os.path.join(dst_dir, file_)
			if os.path.exists(dst_file):
				os.remove(dst_file)
			shutil.copy(src_file, dst_dir)