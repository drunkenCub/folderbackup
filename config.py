#global initiations
COPY_DIR_SRC = {}
COPY_DIR_DEST = {}

BACKUP_DIR_SRC = {}
BACKUP_DIR_DEST = {}

#*********** intranet.bluetag.no ****************#
#directories for deployment 
#make sure to add a / at the end
COPY_DIR_SRC["INTRANETB"] = 'D:/Test/release/'
COPY_DIR_DEST["INTRANETB"] = 'D:/Test/prod_site/'

#directories for backup
#make sure NOT to add a / at the end
BACKUP_DIR_SRC["INTRANETB"] = 'D:/Test/prod_site'  
BACKUP_DIR_DEST["INTRANETB"] = 'D:/Test/backups'

